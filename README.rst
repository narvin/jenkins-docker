*****************
Jenkins in Docker
*****************

CI/CD with a Jenkins controller and cloud agents in Docker containers. If we
use a single Docker host for the controller and the agents, then the controller
would have to connect outwards to its own Docker host to spawn agents, which can
be awkward. Instead, we create another Docker host inside the outer host (Docker
in Docker, or DinD), then have the controller connect laterally to the DinD host
to spawn agents. This better models the situation where we have multiple servers,
and can put the Jenkins controller on one server, and the Docker cloud agents on
another server. In fact, the DinD container could be replaced by Docker hosts on
other servers that are accessible over the network from the Jenkins controller.

The Jenkins controller only exposes its web interface over HTTPS, and the DinD host
only exposes its API over TLS.

We create agent images for Python testing and code quality checks. The testing image
contains the minimum supported minor versions of Python 3, i.e., Python 3.8.0, Python
3.9.0, Python 3.10.0, Python 3.11.0, and Python 3.12.0 at the time of this writing,
as well as Python 3.12.2. The code quality image contains tools to perform linting,
style, and type checking. However, we can use any Docker image as an agent, as long
as it has JDK 17 installed.

Usage
=====

Create a Certificate for the Jenkins Controller
-----------------------------------------------

The Jenkins controller needs a server certificate to operate over HTTPS.

.. code-block:: shell

  openssl req -x509 -newkey rsa:4096 -keyout cert.key -out cert.crt \
    -subj "/CN=jenkins" -days 365 -nodes
  openssl pkcs12 -export -out cert.p12 -inkey cert.key -in cert.crt

#. Enter a password when prompted for the pkcs12 certificate.

#. Place the `cert.p12` file in the `asset/certs/jenkins` directory. You may also
   place the `cert.key` and `cert.crt` files in that directory as well, or delete them.

#. Create a `.env` file in the project root and edit it so it contains
   ``HTTPS_KEY_STORE_PASSWORD=password from step 1``. You can also rename
   `.env.example` and update it with the password.

Build the Jenkins Controller Image
----------------------------------

.. code-block:: shell

  cd src/jenkins-controller
  docker build -t jenkins-blueocean:2.452-jdk17 .

Start the Application Stack
---------------------------

Run `make` from the project root directory.


.. code-block:: shell

  make up

Complete the Initial Setup
--------------------------

#. Get the initial admin password for your Jenkins instance.

   .. code-block:: shell

     docker exec src-jenkins-1 bash -c 'cat $JENKINS_HOME/secrets/initialAdminPassword'

#. Login to the Jenkins controller web interface at https://localhost:8443 with the
   password from the previous step, and complete the initial setup.

Configure a Docker Cloud
------------------------

#. Configure a Docker Cloud.

    * `Docker Host URI`: `tcp://docker:2376`

    * `Server credentials`: Add a new `X.509 Client Certificate`.

      You can use this script to create the credentials. Make sure to run it from
      the project root directory after the DinD client certificate has been created
      in the `asset` directory, e.g., by having previously executed ``make up``.

      .. code-block:: shell

        read -p 'Enter your Jenkins admin username: ' USERNAME
        read -sp 'Password: ' PASSWORD
        crumb="$(curl -fk --cookie-jar /tmp/jenkins-cookie "https://${USERNAME}:${PASSWORD}@localhost:8443/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)")" && \
        curl -kv -H 'content-type:application/xml' -H "${crumb}" --cookie /tmp/jenkins-cookie "https://${USERNAME}:${PASSWORD}@localhost:8443/credentials/store/system/domain/_/createCredentials" --data "
        <org.jenkinsci.plugins.docker.commons.credentials.DockerServerCredentials>
          <id>DockerCert</id>
          <description>Docker Cloud Agent Certificate</description>
          <clientKey>$(<asset/certs/dind/client/key.pem)</clientKey>
          <clientCertificate>$(<asset/certs/dind/client/cert.pem)</clientCertificate>
          <serverCaCertificate>$(<asset/certs/dind/client/ca.pem)</serverCaCertificate>
        </org.jenkinsci.plugins.docker.commons.credentials.DockerServerCredentials>" || printf 'Failed to create credentials\n';$(exit 1)

      Or you can create the credentials manually.

        * `Client Key`: contents of `asset/certs/dind/client/key.pem`.

        * `Client Certificate`: contents of `asset/certs/dind/client/cert.pem`.

        * `Client Certificate`: contents of `asset/certs/dind/client/ca.pem`.

    * Click `Test Connection`.

    * Check `Enabled`.

#. Add Docker Agent templates to the Docker Cloud. You may add any image with JDK 17,
   such as `narvin/jenkins-agent-py3-supported:jdk17-3.8-3.12.2`.

Certificates
^^^^^^^^^^^^

Directories and files from the `asset/certs` directory will be mounted to the
Jenkins controller and DinD containers in order to persist certificates. If certain
files are not present, the DinD container will regenerate its CA, client and server
certificates (see the `_tls_generate_certs` function in `dockerd-entrypoint.sh`_). The
`dind-helper` container makes sure that once DinD certificates are created, they are
not recreated. This way, server credentials based on a DinD client certificate used
to access a Docker Cloud will remain valid upon application stack restarts.

.. _dockerd-entrypoint.sh: https://github.com/docker-library/docker/blob/master/dockerd-entrypoint.sh

Create Multibranch Pipelines with GitLab
----------------------------------------

#. Install the `GitLab Branch Source plugin`_ to have Jenkins manage webhooks
   on GitLab.

    * In `Configure Systems > GitLab Servers` for `Credentials` add a `GitLab Personal
      Access Token` with `API access`_.

    * Check `Manage Web Hooks`.

    * Click `Test connection`.

#. Install the `GitLab plugin`_ so you can use functions like
   `updateGitlabCommitStatus` in your declarative pipelines.

#. Create a `Multibranch Pipeline`.

    * Under `Branch Sources` add a `GitLab Project` source.

    * Enter the `Owner`, then hit tab.

    * `Projects` will populate with the projects from the `Owner`. Select a project.

    * Specify any other options you would like, then click `Save`.

If the project has a `Jenkinsfile`_ in the project root directory, the pipeline
defined in that file will run for each branch in the project now. Jenkins will also
`configure a webhook`_ for the project on GitLab, and the pipeline will run for a
branch whenever it is pushed to the repo.

.. _GitLab Branch Source plugin: https://plugins.jenkins.io/gitlab-branch-source/
.. _API access: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token
.. _GitLab plugin: https://plugins.jenkins.io/gitlab-plugin/
.. _Jenkinsfile: https://gitlab.com/narvin/pydesignpatterns/-/blob/main/Jenkinsfile
.. _configure a webhook: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#configure-a-webhook-in-gitlab

Interact with the Application Stack
-----------------------------------

Exec into the Jenkins controller.

.. code-block:: shell

  make exec-jenkins

Exec into the DinD host.

.. code-block:: shell

  make exec-dind

Stop the application stack.

.. code-block:: shell

  make down
