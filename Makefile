include .env

COMPOSE_FILE = src/docker-compose.yaml
UP = HTTPS_KEY_STORE_PASSWORD="${HTTPS_KEY_STORE_PASSWORD}" \
	docker-compose -f $(COMPOSE_FILE) up -d
DOWN = docker-compose -f $(COMPOSE_FILE) down
LS = docker ps -a
EXEC = docker exec -it

.PHONY: up down

up:
	@$(UP)
	@$(LS)

down:
	@$(DOWN)
	@$(LS)

exec-jenkins:
	@$(EXEC) src-jenkins-1 bash

exec-dind:
	@$(EXEC) src-dind-1 sh
